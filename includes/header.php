<?php require_once('config.php'); ?>

<!DOCTYPE html>
<html ng-app="scraper.app">
<head>
    <meta charset="utf-8" />
    <title></title>
    <link href="css/bootstrap.css" rel="stylesheet" />

    <script src="js/jquery-1.7.2.min.js"></script>
    <script src="js/angular.js"></script>
    <script src="js/angular-animate.min.js"></script>
    <script src="js/app.js"></script>
    <script src="js/ui-bootstrap.min.js"></script>
    <script src="js/ui-bootstrap-tpls.min.js"></script>
</head>

<script type="text/javascript">
    angular.module('data.services', [])
        .service('data', function() {
            return {
                get: function() {
                    return <?php print (isset($angular) && is_array($angular)) ? json_encode($angular) : '{}'; ?>
                }
            };
        })
</script>

<header class="navbar navbar-default navbar-static-top">
    <div style="font-size: 50px">Data</div>
</header>

<body>