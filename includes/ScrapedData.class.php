<?php

require_once('Base.class.php');

class ScrapedData extends Base {
    protected $tableName = 'scraped_data';
    protected $keyName = 'id';

    public static function updateOrCreate($dbh, $properties) {
        $sql = 'insert into scraped_data (id, event_type, location, disposition, event_timestamp)
                values(?, ?, ?, ?, ?)
                on duplicate key update
                  event_type=values(event_type),
                  location=values(location),
                  disposition=values(disposition),
                  event_timestamp=values(event_timestamp)';

        $sth = $dbh->prepare($sql);

        $sth->execute($properties);
        return true;
    }


    public static function find($dbh, $id = null, $filters = array()) {
        $wheres = array();

        $sql = "select * from scraped_data";

        $wheres = $values = array();
        if (!is_null($id)) {
            $wheres[] = 'id = ?';
            $values[] = $id;
        }

        if (isset($filters['start_date'])) {
            $wheres[]  = 'event_timestamp >= ?';
            $values[] = date('Y-m-d 00:00:00', strtotime($filters['start_date']));
        }

        if (isset($filters['end_date'])) {
            $wheres[] = 'event_timestamp <= ?';
            $values[] = date('Y-m-d 23:59:59', strtotime($filters['end_date']));
        }

        if (isset($filters['event_type'])) {
            $wheres[] = 'event_type like ?';
            $values[] = '%' . $filters['event_type'] . '%';
        }

        if (isset($filters['location'])) {
            $wheres[] = 'location like ?';
            $values[] = '%' . $filters['location'] . '%';
        }
        if (isset($filters['disposition'])) {
            $wheres[] = 'disposition like ?';
            $values[] = '%' . $filters['disposition'] . '%';
        }

        if (count($wheres) > 0) {
            $sql .= ' where ';
            foreach ($wheres as $key => $item) {
                if ($key != 0) {
                    $sql .= ' and ';
                }
                $sql .= ' ' . $item . ' ';
            }
        }

        $sql .= ' order by event_timestamp desc';
        $sth = $dbh->prepare($sql);

        $sth->execute($values);

        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function getMinMaxCallsByDay($dbh, $dispositions = array()) {
        $sql = "SELECT MAX(cnt) AS max_cnt, MIN(cnt) AS min_cnt, IF(WEEKDAY(event_timestamp) = 6, 0, WEEKDAY(event_timestamp) + 1) as day_int, event_date
                FROM (
                  SELECT COUNT(id) AS cnt, DATE(event_timestamp) AS event_date, event_timestamp, DAYNAME(event_timestamp) AS day_name
                FROM scraped_data";
        if (count($dispositions) > 0) {
            $sql .= " WHERE disposition IN (?) ";
        }

        $sql .= " GROUP BY DATE(event_timestamp)

                ) max_calls
                GROUP BY day_name
                ORDER BY day_int";

        $sth = $dbh->prepare($sql);
        $sth->execute(array(implode(',', $dispositions)));

        $return = array();
        $data = $sth->fetchAll(PDO::FETCH_ASSOC);
        foreach ($data as $row) {
            $return[$row['day_int']] = $row;
        }

        return $return;
    }
/*
 *
 * SELECT AVG(cnt), day_name
FROM (
SELECT COUNT(id) AS cnt, event_timestamp, DAYNAME(event_timestamp) AS day_name
 FROM scraped_data
 WHERE disposition = 'ARREST'  AND DATE(event_timestamp) >= DATE_SUB(CURRENT_DATE, INTERVAL 3 MONTH)
GROUP BY DATE(event_timestamp)
) max_calls
GROUP BY day_name;
 */
    public static function getAverageCallsByDay($dbh, $dispositions = array(), $months = null) {
        $sql = "SELECT AVG(cnt) as calc_avg, day_name, event_date, IF(WEEKDAY(event_timestamp) = 6, 0, WEEKDAY(event_timestamp) + 1) as day_int
                FROM (
                SELECT COUNT(id) AS cnt, DATE(event_timestamp) AS event_date, event_timestamp, DAYNAME(event_timestamp) AS day_name
                FROM scraped_data";
        $where = $values = array();
        if (count($dispositions) > 0) {
            $where[] = "disposition IN (:disposition)";
            $values[':disposition'] = implode(',', $dispositions);
        }

        if (!is_null($months)) {
            $where[] = 'MONTH(event_timestamp) = MONTH(DATE_SUB(CURRENT_DATE, INTERVAL :months MONTH))';
            $values[':months'] = $months;
        }

        if (count($where) > 0) {
            $sql .= ' WHERE ' . implode(' AND ', $where);
        }

        $sql .= " GROUP BY DATE(event_timestamp)
                ) max_calls
                GROUP BY day_name
                ORDER BY day_int";
        $sth = $dbh->prepare($sql);
        $sth->execute($values);

        $return = array();
        $data = $sth->fetchAll(PDO::FETCH_ASSOC);
        foreach ($data as $row) {
            $return[$row['day_int']] = $row;
        }

        return $return;
    }
}