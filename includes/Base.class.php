<?php

abstract class Base {
    protected $dbh;
    protected $id = null;
    protected $properties = array();
    protected $tableName = null;
    protected $keyName = null;

    public function __construct($dbh, $id) {
        if ( is_null($this->getTableName()) ) {
            throw new Exception('No table name defined for: ' . get_called_class());
        }

        $this->id = $id;
        $this->dbh = $dbh;

        if ( $this->getProperties() === false ) {
            throw new Exception('Error retrieving object ID: ' . $id . ' in ' . get_called_class());
        };
    }

    protected function getTableName() {
        return $this->tableName;
    }

    protected function getKeyName() {
        return $this->keyName;
    }

    protected function getProperties() {
        if ( intval($this->id) == 0 || is_null($this->id) ) {
            return false;
        }

        $sql = 'select * from ' . $this->getTableName() . ' where ' . $this->getKeyName() . ' = ? limit 1';
        $sth = $this->dbh->prepare($sql);
        $sth->execute(array($this->id));

        $rows = $sth->fetchAll(PDO::FETCH_ASSOC);

        if ( count($rows) == 1 ) {
            list($row) = $rows;

            foreach ( $row as $column => $value ) {
                $this->set($column, $value);
            }

            return $this->properties;
        }

        return false;
    }

    public function set($property, $value) {
        $this->properties[$property] = $value;
    }

    public function get($property) {
        if ( isset($this->properties[$property]) ) {
            return $this->properties[$property];
        }

        return null;
    }

    public function key() {
        return $this->id;
    }
}