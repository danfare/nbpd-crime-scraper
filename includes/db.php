<?php

$DBConfig = array(
    "dbhost"     => "127.0.0.1",
    "dbusername" => "root",
    "dbpassword" => "",
    "database"   => "scraper"
);

$dbh = new PDO('mysql:host=' . $DBConfig['dbhost'] . ';dbname=' . $DBConfig['database'] . '', $DBConfig['dbusername'], $DBConfig['dbpassword']);