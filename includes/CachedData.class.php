<?php

require_once('Base.class.php');

class CachedData extends Base {
    protected $tableName = 'cached_data';
    protected $keyName = 'id';

    public static function find($dbh, $day, $slug)
    {
        $sql = "select * from cached_data where day_of_week = :day AND slug = :slug limit 1";
        $sth = $dbh->prepare($sql);

        $sth->execute(array(':day' => $day, ':slug' => $slug));

        $data =  $sth->fetchAll(PDO::FETCH_ASSOC);

        return $data;
    }

    public static function exists($dbh, $day, $slug)
    {
        return (count(CachedData::find($dbh, $day, $slug)) == 1);
    }

    public static function updateOrCreate($dbh, $properties)
    {
        if (!CachedData::exists($dbh, $properties['day_of_week'], $properties['slug'])) {
            $sql = 'insert into cached_data (day_of_week, slug, cache_value)
                    values(:day, :slug, :cache_value)';

        } else {
            $sql = 'update cached_data
                   set cache_value = :cache_value
                   where day_of_week = :day and slug = :slug';
        }

        $values = array(
          ':day' => $properties['day_of_week'],
          ':slug' => $properties['slug'],
          ':cache_value' => $properties['cache_value']
        );

        $sth = $dbh->prepare($sql);
        $result = $sth->execute($values);

        return $result;
    }
}