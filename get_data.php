<?php

define('MAX_FILE_SIZE', 700000);

require_once('includes/simple_html_dom.php');
require_once('includes/db.php');
require_once('includes/helpers.php');
require_once('includes/ScrapedData.class.php');

$start_time = microtime(TRUE);


register_shutdown_function( "fatal_handler" );

function fatal_handler() {
    global $dbh;
    $error = error_get_last();

    //type === 1 we only care about fatal errors
    if ($error !== NULL && is_array($error) && $error['type'] === 1) {
        $start = date('Y-m-d 00:00:00', strtotime("-2 day"));

        $logs = ScrapedData::find($dbh, null, array(
          'start_date' => $start,
          'end_date' => date('Y-m-d 23:59:59', strtotime("-1 day"))
        ));

        if (count($logs) == 0) {
            mail('danfare@gmail.com,michael.glenn@devion.com', 'ERROR! SaveNewport Crime Scraper failed', 'The data scraper process has not returned any data since: ' . $start . "\n" . $error['message'] . "\n\nDebug: " . print_r($error, 1));
            print(date('Y-m-d H:i:s') . " - Data scrape FAILED. Error emailed. \n");

            return false;
        }

        print(date('Y-m-d H:i:s') . " - Data scrape FAILED. Email not sent. " . count($logs) . " records exist since " . $start . "\n");
    }
}



$url = 'http://www.nbpd.org/crime/calls/events.asp?days=7';
$selector = 'table table table table table';
$index = 2; //second selector in DOM

$colMap = array(
    '0' => array(
        'label' => 'Date/Time',
        'column' => '4',
        'sanitize' => function($string) {
            return date('Y-m-d G:i:s', strtotime($string));
        },
        'validate' => function($string) {
            return !($string == '' || strtotime($string) <= strtotime('2013-01-01'));
        }
    ),
    '1' => array(
        'label' => 'Event No.',
        'column' => '0',
        'sanitize' => function($string) {
            return (int) $string;
        }
    ),
    '2' => array(
        'label' => 'Event Type',
        'column' => '1',
        'sanitize' => function($string) {
            return $string;
        }
    ),
    '3' => array(
        'label' => 'Location',
        'column' => '2',
        'sanitize' => function($string) {
            return $string;
        }
    ),
    '4' => array(
        'label' => 'Disposition',
        'column' => '3',
        'sanitize' => function($string) {
            return $string;
        }
    )
);
$ctx = stream_context_create(array('http'=>
  array(
    'timeout' => 600, // 1 200 Seconds = 20 Minutes
  )
));
$html = file_get_html($url, false, $ctx);
$table = $html->find($selector, $index);
$data = array();
$rowCnt = 0;

//array_slice here trims off the first row (table heading)
foreach (array_slice($table->find('tr'), 1) as $row) {
    $colCnt = 0;
    foreach ($row->find('td') as $col) {
        $data[$rowCnt] = (!isset($data[$rowCnt]) || !is_array($data[$rowCnt])) ? array() : $data[$rowCnt];
        if (isset($colMap[$colCnt])) {
            //check the data to make sure it looks right, if it doesn't remove this row from the data
            if (isset($colMap[$colCnt]['validate']) && !$colMap[$colCnt]['validate']($col->plaintext)) {
                unset($data[$rowCnt]);
                $rowCnt--;
                break;
            }

            $data[$rowCnt][$colMap[$colCnt]['column']] = $colMap[$colCnt]['sanitize']($col->plaintext);
        }
        $colCnt++;
    }

    if ($rowCnt > 0 && is_array($data[$rowCnt])) {
        ksort($data[$rowCnt]);
    }
    $rowCnt++;
}

foreach ($data as $event) {
    ScrapedData::updateOrCreate($dbh, $event);
}

$end_time = microtime(TRUE);

// do something...
$html->clear();
unset($html);

print(date('Y-m-d H:i:s') . ' - Data scrape retrieved ' . count($data) . ' new items.....completed in: ' . ($end_time - $start_time) . " seconds \n");


exit;