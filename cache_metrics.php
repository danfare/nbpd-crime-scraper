<?php
require_once('includes/db.php');
require_once('includes/helpers.php');
require_once('includes/ScrapedData.class.php');
require_once('includes/CachedData.class.php');
//create log table
//

$start_time = microtime(TRUE);

$days = range(0,6);

$minMaxCalls = ScrapedData::getMinMaxCallsByDay($dbh);

$minMaxArrests = ScrapedData::getMinMaxCallsByDay($dbh, array('ARREST'));

$callsAvg = ScrapedData::getAverageCallsByDay($dbh);

$arrestsAvg = ScrapedData::getAverageCallsByDay($dbh, array('ARREST'));

$monthCallsAvg = ScrapedData::getAverageCallsByDay($dbh, array(), 1);

$monthArrestsAvg = ScrapedData::getAverageCallsByDay($dbh, array('ARREST'), 1);

foreach ($days as $day) {
    $arrestsMin = array(
      'day_of_week' => $day,
      'slug' => 'arrests-min',
      'cache_value' => $minMaxArrests[$day]['min_cnt']
    );
    CachedData::updateOrCreate($dbh, $arrestsMin);

    $arrestsMax = array(
      'day_of_week' => $day,
      'slug' => 'arrests-max',
      'cache_value' => $minMaxArrests[$day]['max_cnt']
    );
    CachedData::updateOrCreate($dbh, $arrestsMax);

    $callAvg = array(
      'day_of_week' => $day,
      'slug' => 'calls-avg',
      'cache_value' => $callsAvg[$day]['calc_avg']
    );

    CachedData::updateOrCreate($dbh, $callAvg);

    $arrestAvg = array(
      'day_of_week' => $day,
      'slug' => 'arrests-avg',
      'cache_value' => $arrestsAvg[$day]['calc_avg']
    );

    CachedData::updateOrCreate($dbh, $arrestAvg);

    $callsMin = array(
      'day_of_week' => $day,
      'slug' => 'calls-min',
      'cache_value' => $minMaxCalls[$day]['min_cnt']
    );
    CachedData::updateOrCreate($dbh, $callsMin);

    $callsMax = array(
      'day_of_week' => $day,
      'slug' => 'calls-max',
      'cache_value' => $minMaxCalls[$day]['max_cnt']
    );
    CachedData::updateOrCreate($dbh, $callsMax);


    $monthArrestAvg = array(
      'day_of_week' => $day,
      'slug' => 'month-arrests-avg',
      'cache_value' => $monthArrestsAvg[$day]['calc_avg']
    );

    CachedData::updateOrCreate($dbh, $monthArrestAvg);

    $monthCallAvg = array(
      'day_of_week' => $day,
      'slug' => 'month-calls-avg',
      'cache_value' => $monthCallsAvg[$day]['calc_avg']
    );

    CachedData::updateOrCreate($dbh, $monthCallAvg);
}

$end_time = microtime(TRUE);
print(date('Y-m-d H:i:s') . ' - Cached various metrics.....completed in: ' . ($end_time - $start_time) . " seconds \n");


exit;