<?php
require_once('includes/helpers.php');
require_once('includes/db.php');
require_once('includes/ScrapedData.class.php');



//event_type, location, disposition
$filters = array(
    'start_date' => date('Y/m/d', strtotime("-7 days")),
    'end_date' => date('Y/m/d'),
);

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $post = json_decode($_POST['filters'], true);

    $post = array_filter((array) $post);

    $post['start_date'] = date('Y/m/d', strtotime($post['start_date']));
    $post['end_date'] = date('Y/m/d', strtotime($post['end_date']));
    if ($post['start_date'] == '1969/12/31' || $post['end_date'] == '1969/12/31') {
        unset($post['start_date']);
        unset($post['end_date']);
    }

    $filters = array_merge($filters,$post);
}


$angular = array(
    'data' => ScrapedData::find($dbh, null, $filters),
    'filters' => $filters
);

include('includes/header.php');
?>


<script src="js/scraper_data.js"></script>
<div ng-controller="ScraperDataListingController">
    <form action="#" method="post" class="form-horizontal">
        <?php
        /*
        <label for="days" class="control-label col-sm-2">Days:</label>
        <select ng-model="days">
            <option value="all">All</option>
            <?php
            foreach (array('1','2','3','4','5','6','7','14','31','365'))

            ?>
        </select>
        */ ?>
        <div class="row col-md-12">
            <div class="col-md-4">
                <div ng-controller="datepickerController" ng-init="range = 'start'">
                    <div>
                        <label for="start_date" class="control-label col-sm-4">Start Date:</label>
                        <p class="input-group col-md-7">
                            <input type="text" name="start_date" class="form-control" datepicker-popup="{{format}}"
                                   ng-model="$parent.filters.start_date" is-open="opened"
                                   max="'<?php echo date('Y-m-d', strtotime("+1 day")); ?>'"
                                   datepicker-options="dateOptions" date-disabled="disabled(date, mode)"
                                   ng-required="true" close-text="Close" />
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                             </span>
                        </p>
                    </div>
                </div>

                <div ng-controller="datepickerController" ng-init="range = 'end'">
                    <div>
                        <label for="end_date" class="control-label col-sm-4">End Date:</label>
                        <p class="input-group col-md-7">
                            <input type="text" name="end_date" class="form-control" datepicker-popup="{{format}}"
                                   ng-model="$parent.filters.end_date" is-open="opened"
                                   min="$parent.filters.start_date" max="maxDate"
                                   datepicker-options="dateOptions" date-disabled="disabled(date, mode)"
                                   ng-required="true" close-text="Close" />
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button>
                        </span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group  col-sm-10">
                    <label for="event_type" class="control-label col-sm-5">Event Type:</label>
                    <div class="col-sm-6">
                        <input type="text" name="event_type" class="form-control" ng-model="filters.event_type" />
                    </div>
                </div>
                <div class="form-group col-sm-10">
                    <label for="location" class="control-label col-sm-5">Location:</label>
                    <div class="col-sm-6">
                        <input type="text" name="location" class="form-control" ng-model="filters.location" />
                    </div>
                </div>
                <div class="form-group col-sm-10">
                    <label for="disposition" class="control-label col-sm-5">Disposition:</label>
                    <div class="col-sm-6">
                        <input type="text" name="disposition" class="form-control" ng-model="filters.disposition" />
                    </div>
                </div>
                <br />
                <input type="hidden" name="filters" ng-value="filters | json" />
            </div>
        </div>
        <div class="col-md-9 text-center">
            <button type="submit" class="btn btn-primary">Apply Filters</button>
        </div>
        <div class="clearfix"></div>

        <div class="row col-md-9 text-center">
            <div pagination total-items='totalItems' boundary-links="true" max-size="maxSize" ng-model="currentPage" ng-change="setPage(currentPage)"></div>
        </div>



    <table class="table">
        <thead>
        <th>Date/Time</th>
        <th>Event No</th>
        <th>Event Type</th>
        <th>Location</th>
        <th>Disposition</th>
        </thead>
        <?php /* angular filters <tr ng-repeat="item in data | daterange:start_date:end_date | filter:{ event_type: filters.event_type, location: location, disposition: disposition }"> */ ?>
        <tr ng-repeat="item in filteredItems">
            <td>{{ item.event_timestamp }}</td>
            <td>{{ item.id }}</td>
            <td>{{ item.event_type }}</td>
            <td>{{ item.location }}</td>
            <td>{{ item.disposition }}</td>
        </tr>
    </table>
        <div class="row col-md-9 text-center">
            <div pagination total-items='totalItems' boundary-links="true" max-size="maxSize" ng-model="currentPage" ng-change="setPage(currentPage)"></div>
        </div>
    </form>
</div>







<?php
include('includes/footer.php');
?>