var Scraper = angular.module('scraper.app', ['data.services', 'ngAnimate', 'ui.bootstrap', 'ui.bootstrap.tpls']);

Scraper.factory('ajax', function($http){
    return {
        call : function(params){
            return $http({
                url    : params.url,
                method : params.method,
                data   : angular.toJson(params.data)
            });
        }
    }
});



Scraper.directive('validFile',function(){
    return {
        require:'ngModel',
        link:function(scope,el,attrs,ngModel){
            //change event is fired when file is selected
            el.bind('change',function(){
                scope.$apply(function(){
                    ngModel.$setViewValue(el.val());
                    ngModel.$render();
                });
            });
        }
    }
});


/**
 * A generic confirmation for risky actions.
 * Usage: Add attributes: ng-really-message="Are you sure"? ng-really-click="takeAction()" function
 */
Scraper.directive('ngReallyClick', [function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('click', function() {
                var message = attrs.ngReallyMessage;
                if (message && confirm(message)) {
                    scope.$apply(attrs.ngReallyClick);
                }
            });
        }
    }
}]);

Scraper.filter('daterange', function ()
{
  return function(items, start_date, end_date)
  {
    var result = [];

    // date filters
    var start_date = (start_date && !isNaN(Date.parse(start_date))) ? Date.parse(start_date) : 0;
    var end_date = (end_date && !isNaN(Date.parse(end_date))) ? Date.parse(end_date) : new Date().getTime();

    // if the conversations are loaded
    if (items && items.length > 0)
    {
      $.each(items, function (index, item)
      {

        var t = item.event_timestamp.split(/[- :]/);
        var eventTimestamp = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);

        if (eventTimestamp >= start_date && eventTimestamp <= end_date)
        {
          result.push(item);
        }
      });

      return result;
    }
  };
});

