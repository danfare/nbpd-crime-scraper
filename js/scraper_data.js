Scraper.controller('ScraperDataListingController', ['$scope', '$modal', 'data', function($scope, $modal, data) {
  $scope.data = data.get();
  $scope.filters = $scope.data.filters;
  $scope.items = $scope.data.data;

  $scope.filteredItems = []
    ,$scope.currentPage = 1
    ,$scope.numPerPage = 10
    ,$scope.maxSize = 5;

  $scope.boot = function() {

console.log($scope.items);
    $scope.totalItems = $scope.items.length;

    var t = $scope.filters.start_date.split(/[/]/);
    $scope.filters.start_date = new Date(t[0], t[1]-1, t[2], 0, 0, 0);

    var t = $scope.filters.end_date.split(/[/]/);
    $scope.filters.end_date = new Date(t[0], t[1]-1, t[2], 0, 0, 0);

  };


  $scope.setPage = function (pageNo) {
    console.log('setpage');
    $scope.currentPage = pageNo;
    var begin = (($scope.currentPage - 1) * $scope.numPerPage),
      end = begin + $scope.numPerPage;
    console.log("filter");
    $scope.filteredItems = $scope.items.slice(begin, end);
  };

  $scope.setPage($scope.currentPage);

  $scope.boot();
}]);

Scraper.controller('datepickerController', ['$scope', '$modal', 'data', function($scope, $modal, data) {
  $scope.range = null;
  $scope.boot = function() {

    $scope.format = 'yyyy/MM/dd';
    $scope.maxDate = new Date();
  }

  $scope.today = function() {
    $scope.dt = new Date();
  };

  $scope.today();

  $scope.clear = function () {
    $scope.dt = null;
  };

  // Disable weekend selection
  $scope.disabled = function(date, mode) {
    return (false);
  };

  $scope.toggleMin = function() {
    $scope.minDate = $scope.minDate ? null : new Date();
  };
  $scope.toggleMin();

  $scope.open = function($event) {
    $event.preventDefault();
    $event.stopPropagation();

    $scope.opened = true;
  };

  $scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1
  };

  $scope.$parent.$watch("filters['start_date']", function(value) {
    if ($scope.range == 'end') {
      if (Object.prototype.toString.call(value) === '[object Date]') {
        var dt = angular.copy(value);
        dt.setDate(dt.getDate() + 7);

        if (dt > new Date()) {
          dt = new Date();
        }

        $scope.maxDate = dt;
        if ($scope.$parent.filters.end_date > $scope.maxDate) {
          $scope.$parent.filters.end_date = angular.copy($scope.maxDate);
        } else if ($scope.$parent.filters.end_date < value) {
          $scope.$parent.filters.end_date = angular.copy(value);
        }
      }
    }
  });

  $scope.boot();
}]);